import logo from './logo.svg';
import './App.css';
import { useState } from 'react';

import { getDownloadURL, ref, uploadBytes } from 'firebase/storage';
import { storage } from './firebase';

function App() {

  const [ productName, setProductName ] = useState('')
  const [ fileImage, setFileImage ] = useState(null);
   
  const uploadImage = (event) => {
    console.log("masuk upload image")
    let dataFile = event.target.files[0];

    const storageRef = ref(storage, dataFile.name);

    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, dataFile)
    .then((snapshot) => {
      console.log('Uploaded a blob or file!');
    })
    .catch(err => {
      console.log(err);
    })
    .finally(() => {
      getDownloadURL(storageRef)
      .then(downloadUrl => {
        setFileImage(downloadUrl);
      })
    })
  }

  const saveProduct = async (e) => {
    e.preventDefault();
    console.log(productName);
    console.log(fileImage)

    // let formData = new FormData();
    // formData.append('product', productName)
    // formData.append('image', event.target.files[0])

    // ini disesuaikan dengan API kalian
    try {
      let response = await fetch('url:serverkalian', {
        method: 'POST',
        headers: {
          Authorization: `Bearer ` + localStorage.getItem('token') ,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          product: productName,
          // stock: inputanDariUser,
          // price: inputanDariUser,
          productImage: fileImage
        })
      })
    } catch(err) {

    }
  }
 
  return (
    <div className="App">
      <form>
        <input type="text" placeholder="input product name" onChange={(e) => setProductName(e.target.value)}></input>
        <input type="file" onChange={uploadImage}></input>
        <button type="submit" onClick={saveProduct}>Save</button>
      </form>

      <img 
      width={250} height={250}
      src={fileImage}></img>
    </div>
  );
}

export default App;
